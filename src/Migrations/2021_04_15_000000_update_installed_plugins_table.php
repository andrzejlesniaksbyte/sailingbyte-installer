<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateInstalledPluginsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('installed_packages', function (Blueprint $table) {
            $table->string('status')->after('version');
            $table->string('path')->after('status');
            $table->string('serviceProvider')->after('path')->nullable();
            $table->string('namespacePrefix')->after('serviceProvider')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('installed_packages', function (Blueprint $table) {
            $table->dropColumn('status');
            $table->dropColumn('path');
            $table->dropColumn('serviceProvider');
            $table->dropColumn('namespacePrefix');
        });
    }
}
