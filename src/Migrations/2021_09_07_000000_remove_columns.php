<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RemoveColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('installed_packages', function (Blueprint $table) {
            $table->dropColumn("path");
            $table->dropColumn("serviceProvider");
            $table->dropColumn("namespacePrefix");
            $table->dropColumn("order");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
