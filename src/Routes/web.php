<?php

Route::group(['middleware' => ['web']], function () {
    Route::prefix('/update_center')->group(function () {
        Route::get('/', 'Dashboard@dashboard');
        Route::get('/backup/create', 'Backups@create');
        Route::get('/backup/delete', 'Backups@delete');

        Route::get('/disasterScript', 'Update@disasterScript');

        Route::get('/install/{version_id}', 'Update@packageInstall');
        Route::get('/a_install/deleteVendorBackup', 'Update@deleteVendorBackup');
        Route::get('/a_install/copyFiles', 'Update@copyFiles');
        Route::get('/a_install/runMigrations/{version_id}', 'Update@runMigrations');
        Route::get('/a_install/{version_id}', 'Update@ajaxInstall');

        Route::get('/uninstall/{pluginId}', 'Update@uninstall');

        Route::get('/login', 'Dashboard@login');
        Route::post('/login', 'Dashboard@postLogin');
        Route::get('/logout', 'Dashboard@logout');

    });
});
