<?php

function installer_warning_handler($errno, $errstr) {
    if(empty($_SESSION['skipped_files'])) {
        $_SESSION['skipped_files'] = [];
    }
    $_SESSION['skipped_files'][] = $errstr;
}

function updateCenterLoginCheck()
{
    //check if user is logged from laravel
    if(\Auth::user() && \Auth::user()->can('view|update_center')) {
        return true;
    }

    //check if user is logged from update center
    $updateCenterAuth = request()->session()->get('update_center_auth');
    $savedKey = config('installer.license_key');
    if(empty($savedKey)) {
        echo 'Insert license into config installer.php first.';
        exit;
    }

    if($updateCenterAuth == $savedKey) {
        return true;
    }

    redirect('/update_center/login')->send();
    exit;
}

/**
 * Returns library path helper
 * @author JJ
 * @since 2021-08-24
 * @param null|string $path
 * @return string
 * @throws BindingResolutionException
 */
function library_path(?string $path = null)
{
    $relativePath = "libraries/".$path;
    return base_path($relativePath);
}