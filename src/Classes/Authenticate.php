<?php 

namespace Sailingbyte\Installer\Classes;

class Authenticate
{

    private $password = '[PASSWORD]';

    /**
     * Simple auth for disaster script purpose
     * @author JJ
     * @since 2021-04-15
     * @return
     */
    public static function check()
    {
        //check if user click logout
        $logout = $_REQUEST['logout'] ?? null;
        if (!empty($logout)) {
            $_SESSION['update_center_auth'] = null;
            header('Location: /');
            exit;
        }

        //check if we have password in request
        $passwordInRequest = $_REQUEST['password'] ?? null;
        //if not empty - put into session
        if (!empty($passwordInRequest)) {
            $_SESSION['disaster_auth'] = $passwordInRequest;
        }

        //check if already logged in
        $check = $_SESSION['disaster_auth'] ?? null;
        if (empty($check)) {
            self::showLoginForm();
        }
        if (!password_verify($check, self::$password)) {
            self::showLoginForm();
        }
    }

    /**
     * Show login form
     * @author JJ
     * @since 2021-04-15
     * @return
     */
    public static function showLoginForm()
    {
        echo '
        <form method="post">
            <input name="password">
            <input type="submit" value="Submit">
        </form>
        ';
        die;
    }

}