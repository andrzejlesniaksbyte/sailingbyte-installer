<?php
namespace Sailingbyte\Installer\Classes\LibrariesManager;

class LibrariesManager
{
    private $libraryFolder = null;

    /**
     * constructor
     * @author JJ
     * @since 2021-03-16
     */
    public function __construct() 
    {
        $this->libraryFolder = base_path().'/libraries';
        if(!file_exists($this->libraryFolder)) {
            mkdir($this->libraryFolder, 0777);
        }
    }
    
    /**
     * Init libraries manager 
     * @author JJ
     * @since 2021-03-16
     * @return void
     */
    public function init() 
    {
        
    }   

}
