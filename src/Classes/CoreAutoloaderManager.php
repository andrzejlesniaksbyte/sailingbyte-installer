<?php
namespace Sailingbyte\Installer\Classes;

use Illuminate\Database\Eloquent\Model;
use Sailingbyte\Installer\DataModels\Plugin;

class CoreAutoloaderManager
{
    private $plugins = [];

    private $dependencyProviders = [];

    /**
     * register plugin - or plugins
     * @author JJ
     * @since 2021-03-16
     * @param string $namespace
     * @param string $serviceProvider
     * @param int $priority
     * @return self 
     */
    public function registerPlugin(string $namespace, string $serviceProvider, int $priority = 10) : self
    {
        $plugin = new Plugin($namespace, $serviceProvider);        
        if(!isset($this->plugins[$priority])) {
            $this->plugins[$priority] = [];
        }
        $this->plugins[$priority][] = $plugin;
        return $this;        
    }
    
    /**
     * initialize autoloader manager load
     * @author JJ
     * @since 2021-03-16
     * @return  
     */
    public function init() : void
    {
        //get env vars
        $app = app();
        $autoloadPath = base_path() . '/vendor/autoload.php';
        $mainAutoloader = require $autoloadPath;

        //sort order
        ksort($this->plugins);

        //loop through each plugin and initialize them
        foreach($this->plugins as $pluginByPriority) {
            foreach($pluginByPriority as $plugin) {
                //set PSR4 loading for plugin
                $mainAutoloader->setPsr4($plugin->namespaceInvoke, $plugin->sourcePath);

                //include their vendor paths if exists
                if(!empty($plugin->vendorPath)) {
                    include_once $plugin->vendorPath."/autoload.php";
                }

                //register new service providers
                $app->register($plugin->serviceProvider);

                $plugin->setStatus('initialized');
            }
        }
    }

    /**
     * force autoload plugin
     * @author JJ
     * @since 2021-03-16
     * @param string $serviceProvider
     * @return void
     */
    public function loadDependencyProvider(string $serviceProvider): void
    {
        //check if dependency has been already loaded
        if(in_array($serviceProvider, $this->dependencyProviders)) {
            throw new \Exception('Dependency '.$serviceProvider.' already loaded.');
        }

        $app = app();
        $app->register($serviceProvider);

        //dump information about class to easier debug
        $data = new \StdClass();
        $data->reflectionClass = new \ReflectionClass($serviceProvider);
        $data->filename = $data->reflectionClass->getFileName();

        $this->dependencyProviders[$serviceProvider] = $data;
    }
    
}
