<?php
namespace Sailingbyte\Installer\Classes;

class ApiCall
{
    public static function prepareUrl($endpoint)
    {
        //make url
        $url = config('installer.update_server');
        //add authorization parameter

        $endpoint = parse_url($endpoint);

        if(empty($endpoint['path'])) {
            throw new \Exception('Invalid API Query.');
        }
        $parameters = [];
        if(!empty($endpoint['query'])) {
            //try to decode query
            parse_str($endpoint['query'], $parameters);
            if(isset($parameters['key'])) {
                throw new \Exception('Parameter "Key" cannot be used in query.');
            }
        }
        //add authorization param
        $parameters['key'] = config('installer.license_key');

        $url .= $endpoint['path'].'?'.http_build_query($parameters);

        return $url;
    }

    /**
     * call api endpoint
     * @author JJ
     * @since 2021-04-14
     * @return
     */
    public static function call($endpoint)
    {
        if (empty(config('installer.license_key'))) {
            throw new \Exception('Invalid License Key. Insert valid license key in /config/installer.php file.');
        }

        $curl = curl_init(self::prepareUrl($endpoint));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT , 300);
        curl_setopt($curl, CURLOPT_TIMEOUT, 300);
        $result = curl_exec($curl);
        $decodedResult = json_decode($result);

        if (empty($decodedResult) || $decodedResult->statusCode == 0) {
            throw new \Exception($decodedResult->message ?? 'Undefined API error : '.$result);
        }

        return $decodedResult;
    }

}
