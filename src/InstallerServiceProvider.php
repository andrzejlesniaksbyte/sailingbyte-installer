<?php
namespace Sailingbyte\Installer;

use Illuminate\Support\ServiceProvider;
use Sailingbyte\Installer\App\Models\InstalledPackages;
use Sailingbyte\Installer\Classes\CoreAutoloaderManager;
use Sailingbyte\Installer\Classes\LibrariesManager\LibrariesManager;
use Sailingbyte\Installer\App\Http\Controllers\Update;
use Illuminate\Support\Facades\Schema;

class InstallerServiceProvider extends ServiceProvider
{

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    public function boot()
    {
        Schema::defaultStringLength(191);

        require_once(__DIR__.'/helper.php');

        $this->app->router->group(['namespace' => '\Sailingbyte\Installer\App\Http\Controllers'], function ($router) {
            require __DIR__.'/Routes/web.php';
        });

        $this->loadViewsFrom(__DIR__.'/Resources', 'Installer');
        $this->loadMigrationsFrom(__DIR__.'/Migrations');

        $this->publishes([
            __DIR__.'/Public' => public_path('/'),
            __DIR__.'/Config/installer.php' => config_path('installer.php'),
        ]);
    }

    /**
     * register config
     * @param none
     * @return none
     */
    public function register()
    {
        //initialize libraries manager
        $this->app->singleton('LibrariesManager', function ($app) {
            return new LibrariesManager($app);
        });

        $librariesManager = new LibrariesManager();
        $librariesManager->init();

        //initialize autoloader manager
        $this->app->singleton('CoreAutoloaderManager', function ($app) {
            return new CoreAutoloaderManager($app);
        });

        $autoloadManager = \App::make('CoreAutoloaderManager');

        //get loader data
        if(file_exists(base_path('libraries/loader.json'))) {
            $loaderData = json_decode(file_get_contents(base_path('libraries/loader.json')));
            if(!empty($loaderData)) {
                foreach($loaderData as $key => $package) {
                    try {
                        $autoloadManager->registerPlugin($package->namespacePrefix, $package->serviceProvider);
                    } catch(\Throwable $e) {
                        echo 'Invalid package '.$package->name;
                    }
                }
            }
        }
        $autoloadManager->init();

        //BACKUPS CONFIGURATION
        //disable excluding vendor from backup
        $excludedBackups = [
            'vendor'
        ];
        $backupExcludes = config('backup.backup.source.files.exclude');
        if(!empty($backupExcludes)) {
            foreach($excludedBackups as $excludedBackup) {
                if (($key = array_search(base_path($excludedBackup), $backupExcludes)) !== false) {
                    unset($backupExcludes[$key]);
                }
            }
        }
        config(['backup.backup.source.files.exclude' => $backupExcludes]);
        config(['backup.backup.name' => 'backup']);
    }
}
