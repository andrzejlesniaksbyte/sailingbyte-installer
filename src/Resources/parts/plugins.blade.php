
<h2 class="uk-margin-top">Installed Plugins</h2>
<table class="uk-table sb-table">
    <thead>
        <tr>
            <th>Name</th>
            <th>Version</th>
            <th>Status</th>
            <th class="uk-text-center">Action</th>
        </tr>
    </thead>
    <tbody>
        @if(!$installedPlugins->isEmpty())
            @foreach($installedPlugins as $plugin)
                <tr>
                    <td>{{ $plugin->name }}</td>
                    <td>
                        {{ $plugin->version }}
                        @if(isset($availableUpdates[$plugin->namespace]))
                            - <span class="uk-text-danger">Update available - {{ $availableUpdates[$plugin->namespace]['version'] }}</span>
                        @endif
                    </td>
                    <td>{{ $plugin->status }}</td>
                    <td class="uk-text-center">
                        @if(isset($availableUpdates[$plugin->namespace]))
                            <a href="/update_center/install/{{ $availableUpdates[$plugin->namespace]['version_id'] }}">
                                <i uk-tooltip="title: Update plugin" class="fas fa-sync"></i>
                            </a>
                        @endif
                        <a href="/update_center/uninstall/{{ $plugin->id }}">
                            <i uk-tooltip="title: Delete plugin" class="fas fa-trash-alt"></i>
                        </a>
                    </td>
                </tr>
            @endforeach
        @endif
    </tbody>
</table>

<h2 class="uk-margin-top">Available Plugins</h2>
<table class="uk-table sb-table">
    <thead>
        <tr>
            <th>Name</th>
            <th>Version</th>
            <th class="uk-text-center">Action</th>
        </tr>
    </thead>
    <tbody>
        @if(!empty($pluginsToInstall))
            @foreach($pluginsToInstall as $plugin)
            <tr>
                <td>{{ $plugin->name }}</td>
                <td>{{ $plugin->version }}</td>
                <td class="uk-text-center">
                    <a href="/update_center/install/{{ $plugin->version_id }}">
                        <i uk-tooltip="title: Install plugin" class="fas fa-download"></i>
                    </a>
                </td>
            </tr>
            @endforeach
        @endif
    </tbody>
</table>
