@extends('Installer::layout')

@section('header')
@endsection

@section('content')
<div>
    <h2 class="sb-color__white">Backups - Create</h2>
    {!! nl2br($data) !!}
</div>
@endsection