<h2>Backups</h2>
<table class="uk-table sb-table">
    <thead>
        <tr>
            <th>Name</th>
            <th class="uk-text-center">Action</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($backups as $backup)
            <tr>
                <td>{{ $backup }}</td>
                <td class="uk-text-center">
                    <a href="/update_center/backup/delete?backup={{ base64_encode($backup) }}">
                        <i uk-tooltip="title: Delete backup" class="fas fa-trash-alt"></i>
                    </a>
                </td>
            </tr>
        @endforeach
    </tbody>
</table>
<a class="uk-button sb-btn__main" href="/update_center/backup/create">Create Backup</a>