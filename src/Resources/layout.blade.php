<html>
    <head>
        <title>Sailing Byte - Installer</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="/installer/css/uikit.min.css" />
        <link rel="stylesheet" href="/installer/css/main.css" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css">
        <script src="/installer/js/jquery-3.6.0.min.js"></script>
        <script src="/installer/js/uikit.min.js"></script>
        <script src="/installer/js/uikit-icons.min.js"></script>
        @yield('header')
    </head>
    <body>
        @yield('content')
    </body>
</html>