@extends('Installer::layout')

@section('header')
@endsection

@section('content')
<div class="sb-container__main-wrapper">
    <div class="sb-menu__main-wrapper">
        <div class="uk-container uk-flex uk-flex-between">
            <h1 class="uk-margin-remove sb-color__white">Updater</h1>
            <a class="sb-btn__main uk-button" href="{{ url('/update_center/logout') }}">Logout</a>
        </div>
    </div>
    <div class="uk-container uk-margin-top">
        @include('Installer::parts.backups')
        <hr>
        @include('Installer::parts.plugins')
    </div>
</div>
@endsection