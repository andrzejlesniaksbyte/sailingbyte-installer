@extends('Installer::layout')

@section('header')
@endsection

@section('content')
    <div class="sb-container__main-wrapper">
        <div class="sb-menu__main-wrapper">
            <div class="uk-container uk-flex uk-flex-right">
                <a class="sb-btn__main uk-button" href="/update_center">Return to update center</a>
            </div>
        </div>
        <div id="sb-container__modal-wrapper">
    
            @if (file_exists(base_path('composer_backups/vendor')))
                <div id="old_vendor_backup" class="uk-flex-top" uk-modal>
                    <div class="uk-modal-dialog uk-modal-body uk-margin-auto-vertical sb-box__main-wrapper sb-border__thick">

                        <div id="disaster_question">
                            <p>Old vendor_backup folder still exist. If you want to proceed with backup you need to delete it.
                                Do you want to do it now?</p>
                            <div class="uk-flex uk-flex-center">
                                <button id="delete_vendor_backup" class="uk-button uk-button-primary sb-btn__main uk-margin-small-right">Yes</button>
                                <button class="stop_install uk-button uk-button-primary sb-btn__danger">Stop Installation</button>
                            </div>
                        </div>

                        <div id="disaster_password_question" style="display:none;">
                            <div>
                                <label class="uk-form-label" for="password">
                                    <h2 class="uk-text-center sb-color__white uk-margin-small-bottom">Create Password</h2>
                                </label>
                                <div class="uk-form-controls">
                                    <input class="uk-input sb-border__thin" type="text" name="password" id="password" value="">
                                </div>
                                <p class="uk-text-center">Script will be available at url <a href="{{ url('/disaster_script.php') }}">{{ url('/disaster_script.php') }}</a></p>
                            </div>
                            <div class="uk-flex uk-flex-center">
                                <button id="send_disaster_password" class="uk-button uk-button-primary sb-btn__main uk-margin-small-right">Create</button>
                                <button class="revoke_disaster uk-button uk-button-primary sb-btn__main uk-margin-small-right">Skip</button>
                                <button class="stop_install uk-button uk-button-primary sb-btn__danger">Stop Installation</button>
                            </div>
                        </div>
                    </div>
                </div>
                <script>
                    jQuery(document).ready(function() {
                        UIkit.modal("#old_vendor_backup").show();
    
                        jQuery("#delete_vendor_backup").click(function() {
                            var url = "/update_center/a_install/deleteVendorBackup";
                            jQuery.get({
                                    url: url
                                })
                                .done(function() {
                                    UIkit.modal("#old_vendor_backup").hide();
                                    UIkit.modal("#disaster_modal").show();
                                })
                                .fail(function() {
                                    var results =
                                        "Error during file deletion. Probably you don't have permission to that files. Please change permission and try again.";
                                    UIkit.modal("#old_vendor_backup").hide();
                                    UIkit.modal("#install_modal").show();
                                    jQuery("#install_results_text").html(results);
                                    jQuery('#install_results_controls').show();
                                });
                        });
                    })
                </script>
            @else
                <script>
                    jQuery(document).ready(function() {
                        UIkit.modal("#disaster_modal").show();
                    })
                </script>
            @endif
    
    
            <div id="disaster_modal" class="uk-flex-top" uk-modal>
                <div class="uk-modal-dialog uk-modal-body uk-margin-auto-vertical sb-box__main-wrapper sb-border__thick">

                    <div id="disaster_question">
                        <p class="uk-text-center">Do you want to create disaster recovery script?</p>
                        <div class="uk-flex uk-flex-center">
                            <button id="create_disaster" class="uk-button uk-button-primary sb-btn__main uk-margin-small-right">Yes</button>
                            <button class="revoke_disaster uk-button uk-button-primary sb-btn__main uk-margin-small-right">No</button>
                            <button class="stop_install uk-button uk-button-primary sb-btn__danger">Stop Installation</button>
                        </div>
                    </div>

                    <div id="disaster_password_question" style="display:none;">
                        <div>
                            <label class="uk-form-label sb-color__white" for="password">
                                <h2 class="uk-text-center sb-color__white uk-margin-small-bottom">Create Password</h2>
                            </label>
                            <div class="uk-form-controls">
                                <input class="uk-input sb-border__thin" type="password" name="password" id="password" value="">
                            </div>
                            <p class="uk-text-center">Script will be available at url <a href="{{ url('/disaster_script.php') }}">{{ url('/disaster_script.php') }}</a></p>
                        </div>
                        <div class="uk-flex uk-flex-center">
                            <button id="send_disaster_password" class="uk-button uk-button-primary sb-btn__main uk-margin-small-right">Create</button>
                            <button class="revoke_disaster uk-button uk-button-primary sb-btn__main uk-margin-small-right">Skip</button>
                            <button class="stop_install uk-button uk-button-primary sb-btn__danger">Stop Installation</button>
                        </div>
                    </div>
                </div>
            </div>
    
            <div id="backup_modal" class="uk-flex-top" uk-modal>
                <div class="uk-modal-dialog uk-modal-body uk-margin-auto-vertical sb-box__main-wrapper sb-border__thick">

                    <div id="backup_question">
                        <p class="uk-text-center">Do you want to create backup?</p>
                        <div class="uk-flex uk-flex-center">
                            <button id="start_backup" class="uk-button uk-button-primary sb-btn__main uk-margin-small-right">Yes</button>
                            <button id="revoke_backup" class="uk-button uk-button-primary sb-btn__main uk-margin-small-right">No</button>
                            <button class="stop_install uk-button uk-button-primary sb-btn__danger">Stop Installation</button>
                        </div>
                    </div>

                    <div id="backup_results" style="display:none;">
                        <div id="backup_results_text" class="uk-text-center">
                            Backup pending...
                            <div class="uk-margin-small-left" uk-spinner></div>
                        </div>
                        <div id="backup_results_controls" class="uk-flex uk-flex-center uk-margin-top" style="display:none;">
                            <button id="backup_continue" class="uk-button uk-button-primary sb-btn__main uk-margin-small-right">Continue</button>
                            <button class="stop_install uk-button uk-button-primary sb-btn__danger">Stop Installation</button>
                        </div>
                    </div>

                </div>
            </div>
    
            <div id="install_modal" class="uk-flex-top" uk-modal>
                <div class="uk-modal-dialog uk-modal-body uk-margin-auto-vertical sb-box__main-wrapper sb-border__thick">

                    <div id="install_results_text" class="uk-text-center">
                        <p>Installing in progress...</p>
                        <p>Creating updated vendor...</p>
                    </div>
                    <div class="uk-width-1-1 uk-flex uk-flex-center">
                        <div class="uk-margin-small-top" id="spinner" uk-spinner></div>
                    </div>
                    <div id="install_results_controls" class="uk-flex uk-flex-center uk-margin-top" style="display:none;">
                        <button class="stop_install uk-button uk-button-primary sb-btn__main">Finish installation</button>
                    </div>

                </div>
            </div>
    
    
            <script>
                jQuery(document).ready(function() {
                    jQuery('#create_disaster').click(function() {
                        jQuery('#disaster_question').hide();
                        jQuery('#disaster_password_question').show();
                    });
    
                    jQuery('.revoke_disaster').click(function() {
                        UIkit.modal("#disaster_modal").hide();
                        UIkit.modal("#backup_modal").show();
                    });
    
                    jQuery('#send_disaster_password').click(function() {
                        var password = jQuery('#password').val();
                        if (password.length < 6) {
                            alert('Password is too short! Minimum 6 characters needed.');
                            return false;
                        }
                        jQuery.get("/update_center/disasterScript?password=" + password, function(results) {
                            UIkit.modal("#disaster_modal").hide();
                            UIkit.modal("#backup_modal").show();
                        })
                    });
    
                    jQuery("#start_backup").click(function() {
                        jQuery('#backup_question').hide();
                        jQuery('#backup_results').show();
    
                        //ajax request
                        jQuery.get("/update_center/backup/create", function(results) {
                            jQuery("#backup_results_text").html(results);
                            jQuery('#backup_results_controls').show();
                        })
                    });
    
                    jQuery("#backup_continue").click(function() {
                        install();
                    });
    
                    jQuery("#revoke_backup").click(function() {
                        install();
                    });
    
    
                    function install() {
                        UIkit.modal("#backup_modal").hide();
                        UIkit.modal("#install_modal").show();
    
                        //install vendor
                        var url = "/update_center/a_install/{{ $version_id }}";
                        //var url = "/update_center/";
                        jQuery.get({
                                url: url
                            })
                            .done(function() {
                                jQuery("#install_results_text").append("<p>Copying new files...</p>");
                                copyFiles();
                            })
                            .fail(function() {
                                var results = "Error during installation. Please try again.";
                                jQuery("#install_results_text").html(results);
                                jQuery('#install_results_controls').show();
                                jQuery('#spinner').hide();
                            });
                    }
    
                    function copyFiles() {
                        var url = "/update_center/a_install/copyFiles";
                        //var url = "/update_center/";
                        jQuery.get({
                                url: url
                            })
                            .done(function() {
                                jQuery("#install_results_text").append(
                                    "<p>Running migrations && Post-update scripts...</p>");
                                runMigrations();
                            })
                            .fail(function() {
                                var results = "Error during installation. Please try again.";
                                jQuery("#install_results_text").html(results);
                                jQuery('#install_results_controls').show();
                                jQuery('#spinner').hide();
                                result = false;
                            });
    
                        return result;
                    }
    
                    function runMigrations() {
                        var url = "/update_center/a_install/runMigrations/{{ $version_id }}";
                        //var url = "/update_center/";
                        jQuery.get({
                                url: url
                            })
                            .done(function() {
                                var results = "Installation finished successfully";
                                jQuery("#install_results_text").html(results);
                                jQuery('#install_results_controls').show();
                                jQuery('#spinner').hide();
                            })
                            .fail(function() {
                                var results = "Error during installation. Please try again.";
                                jQuery("#install_results_text").html(results);
                                jQuery('#install_results_controls').show();
                                jQuery('#spinner').hide();
                            });
                    }
    
                    jQuery('.stop_install').click(function() {
                        window.location.href = "/update_center";
                    });
                });
            </script>
        </div>
    </div>
@endsection
