@extends('Installer::layout')

@section('header')
@endsection

@section('content')
<div class="sb-container__main-wrapper">
    <div class="uk-container uk-flex uk-flex-middle uk-flex-center" uk-height-viewport>
        <form action="{{ url('update_center/login') }}" method="POST">
            {{ csrf_field() }}
            <div class="sb-box__main-wrapper sb-border__thick">
                <label class="uk-form-label uk-text-center" for="license_key"><h1 class="sb-color__white">Enter your license key:</h1></label>
                <div class="uk-width-1-1 uk-margin-top uk-text-center">
                    <div class="uk-form-controls">
                        <input class="uk-input sb-border__thin" type="password" name="license_key" id="license_key" value="">
                    </div>
                    <button class="sb-btn__main uk-button uk-button-primary uk-margin-small-top" type="submit">Login</button>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection