<?php
namespace Sailingbyte\Installer\DataModels;

class Plugin
{
    private $status = 'new';

    private $version = 0;

    private $subVersion = 0;

    private $releaseNumber = 0;

    private $namespace = null;

    private $namespaceInvoke = null;

    private $vendorName = null;

    private $packageName = null;

    private $serviceProvider = null;

    private $composerData = null;

    private $basePath = null;

    private $sourcePath = null;

    private $vendorPath = null;

    private $docPath = null;

    private $definedPathPrefixes = [
        'libraryPrefix' => 'libraries',
        'sourcePath' => 'src',
        'vendorPath' => 'vendor',
        'docPath' => 'doc',
        'composerFile' => 'composer.json',
    ];

    private $allowedStatuses = [
        'new',
        'initialized'
    ];

    /**
     * constructor
     * @author JJ
     * @since 2021-03-16
     * @param string $namespace
     */
    public function __construct(string $namespace, $serviceProvider)
    {
        $this->validateNamespace($namespace, $serviceProvider);
        $this->setPaths();
        $this->getComposerData();
    }

    /**
     * validate and set namespace
     * @author JJ
     * @since 2021-03-16
     * @param string $namespace
     * @param string $serviceProvider
     * @return void
     */
    public function validateNamespace(string $namespace, string $serviceProvider): void
    {
        if (empty($namespace)) {
            throw new \Exception('Namespace cannot be null.');
        }

        //explode and remove empty elements.
        //path should contain "vendorName/packageName"
        $names = explode('/', $namespace);
        $names = array_filter($names, 'strlen');

        if (count($names) != 2) {
            throw new \Exception('Invalid namespace name. Path should contain "vendorName/packageName"');
        }

        //set vars
        $this->namespace = $namespace;
        $this->vendorName = $names[0];
        $this->packageName = $names[1];
        $this->namespaceInvoke = $this->vendorName."\\".$this->packageName."\\";
        $this->serviceProvider = '\\'.$this->namespaceInvoke.$serviceProvider;
    }

    /**
     * set plugin paths
     * @author JJ
     * @since 2021-03-16
     * @return void
     */
    public function setPaths(): void
    {
        $requiredPaths = [
            'basePath',
            'sourcePath',
        ];

        $pathsToValidate = [
            'basePath',
            'sourcePath',
            'vendorPath',
            'docPath',
        ];

        //set paths
        $this->basePath = base_path() . '/' . $this->definedPathPrefixes['libraryPrefix'] . '/' . strtolower($this->namespace);
        $this->sourcePath = base_path() . '/' . $this->definedPathPrefixes['libraryPrefix'] . '/' . strtolower($this->namespace) . '/' . $this->definedPathPrefixes['sourcePath'];
        $this->vendorPath = base_path() . '/' . $this->definedPathPrefixes['libraryPrefix'] . '/' . strtolower($this->namespace) . '/' . $this->definedPathPrefixes['vendorPath'];
        $this->docPath = base_path() . '/' . $this->definedPathPrefixes['libraryPrefix'] . '/' . strtolower($this->namespace) . '/' . $this->definedPathPrefixes['docPath'];

        //..and validate them
        foreach ($pathsToValidate as $validatedPath) {
            if (!file_exists($this->{$validatedPath})) {

                if (in_array($validatedPath, $requiredPaths)) {
                    throw new \Exception('Path "' . $this->{$validatedPath} . '" for ' . $this->namespace . ' does not exists.');
                }

                $this->{$validatedPath} = null;
            }
        }
    }

    /**
     * Get composer data && version
     * @author JJ
     * @since 2021-03-16
     * @return void
     */
    public function getComposerData(): void
    {
        //check if composer.phar exists
        $composerPath = $this->basePath . '/' . $this->definedPathPrefixes['composerFile'];
        if(file_exists($composerPath)) {
            $composer = file_get_contents($composerPath);
            $composer = json_decode($composer);
            $this->composerData = $composer;
            if (isset($composer->version)) {
                
                $version = explode('.', $composer->version);
                $this->version = $version[0] ?? 0;
                $this->subVersion = $version[1] ?? 0;
                $this->releaseNumber = $version[2] ?? 0;

            }
        }
    }

    /**
     * get status
     * @author JJ
     * @since 2021-03-16
     * @return string
     */
    public function status() : string
    {
        return $this->status;
    }

    /**
     * change status
     * @author JJ
     * @since 2021-03-16
     * @param string $status
     * @return void
     */
    public function setStatus(string $status) : void
    {
        if(in_array($status, $this->allowedStatuses)) {
            $this->status = $status;
        } else {
            throw new \Exception('Invalid status '.$status.' for plugin '.$this->namespace);
        }
    }
    
    /**
     * magic method for get
     * @author JJ
     * @since 2021-03-16
     * @param string $name
     */
    public function __get(string $name)
    {
        return $this->{$name};
    }
    
    /**
    * magic method for isset
     * @author JJ
     * @since 2021-03-16
    */
    public function __isset(string $name) 
    {
        return isset($this->{$name});
    }
}

