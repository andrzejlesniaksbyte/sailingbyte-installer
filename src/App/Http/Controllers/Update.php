<?php

namespace Sailingbyte\Installer\App\Http\Controllers;

use Illuminate\View\View;
use Illuminate\Http\Request;
use Symfony\Component\Process\Process;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Schema;
use Sailingbyte\Installer\Classes\ApiCall;
use Sailingbyte\Installer\App\Models\InstalledPackages;

use function Ramsey\Uuid\v1;

class Update
{
    /**
     * perform laravel update
     * @author JJ
     * @since 2021-03-16
     * @return
     */
    public function packageInstall($version_id)
    {
        updateCenterLoginCheck();

        //check permission
        $unwritableFiles = self::checkWritablePermission(base_path('/vendor'));
        if (!empty($unwritableFiles)) {
            echo '<h1>Pacakge installation aborted. There are permission problems with files:</h1>';
            foreach ($unwritableFiles as $unwritableFile) {
                echo $unwritableFile . "<br>";
            }
            die;
        }

        return view("Installer::install", compact('version_id'));
    }

    /**
     * Ajax installation
     * @author JJ
     * @since 2021-04-14
     * @return
     */
    public function ajaxInstall(Request $request, $version_id)
    {
        updateCenterLoginCheck();

        $tmpPath = storage_path('update_tmp');
        //prepare tmp folder for composer update
        if (!file_exists($tmpPath)) {
            mkdir($tmpPath, 0777, true);
        }

        //move current composer json & lock to this place
        copy(base_path("composer.json"), $tmpPath . "/composer.json");
        copy(base_path("composer.lock"), $tmpPath . "/composer.lock");

        //get composer lock && composer json of package to prepare composer.lock file
        $packageData = ApiCall::call('/getPackageData?version=' . $version_id);

        if (empty($packageData->data->composerJson) || empty($packageData->data->composerLock)) {
            echo 'Package does not contain composer data.';
            die;
        }

        //make merge of composer files
        $needComposerInstall = false;
        [$package, $laravel, $changes] = $this->getDependenciesData($packageData);
        $decodedComposerJson = json_decode(base64_decode($packageData->data->composerJson));


        //try to assess which packages already exists in composer - if not, we should add it
        foreach ($package->json->require as $key => $elem) {
            if (!isset($laravel->json->require->{$key})) {
                $laravel->json->require->{$key} = $elem;
            }
        }


        //check & add repository that installation is in progress
        $laravel->json->require->{$decodedComposerJson->name} = $decodedComposerJson->version;

        //recreate repository record
        if (isset($laravel->json->repositories)) {
            foreach ($laravel->json->repositories as $key => $repository) {
                if (isset($repository->type) && $repository->type == "git") {
                    if (isset($repository->url) && (stripos($repository->url, $decodedComposerJson->name) !== false)) {
                        unset($laravel->json->repositories[$key]);
                    }
                }

                if (isset($repository->type) && $repository->type == "package") {
                    if (isset($repository->package->name) && ($repository->package->name == $decodedComposerJson->name)) {
                        unset($laravel->json->repositories[$key]);
                    }
                }
            }
        }

        $newRecord = new \StdClass();
        $newRecord->type = "package";
        $newRecord->package = new \StdClass();
        $newRecord->package->name = $decodedComposerJson->name;
        $newRecord->package->version = $decodedComposerJson->version;
        $newRecord->package->dist = new \StdClass();
        $newRecord->package->dist->url = ApiCall::prepareUrl("/getArchive?version=".$version_id);
        $newRecord->package->dist->type = "zip";
        if (!isset($laravel->json->repositories)) {
            $laravel->json->repositories = [];
        }

        $laravel->json->repositories[] = $newRecord;

        $laravel->json->repositories = array_values($laravel->json->repositories);

        // save composer.json
        file_put_contents($tmpPath . '/composer.json', json_encode($laravel->json, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT));

        //parse packages scripts to more easily manage
        $packageParsedPackages = $this->parsePackages($package->lock->packages);
        $laravelParsedPackages = $this->parsePackages($laravel->lock->packages);

        //get all conflicts && replaces
        $conflicts = [];
        $replaces = [];
        foreach ($laravelParsedPackages as $key => $elem) {
            if (isset($elem->conflict) && !empty($elem->conflict)) {
                foreach ($elem->conflict as $conflictKey => $conflictElem) {
                    $conflicts[] = $conflictKey;
                }
            }
            if (isset($elem->replace) && !empty($elem->replace)) {
                foreach ($elem->replace as $replaceKey => $replaceElem) {
                    $replaces[] = $replaceKey;
                }
            }
        }

        //add to lock if can
        foreach ($packageParsedPackages as $key => $elem) {
            if (!isset($laravelParsedPackages[$key])) {
                //check if there is no conflict or replacement
                if (in_array($key, $conflicts)) {
                    continue;
                }
                if (in_array($key, $replaces)) {
                    continue;
                }

                //strip requirements
                if (isset($elem->require) && !empty($elem->require)) {
                    unset($elem->require);
                }

                //insert into lock if there is no conflicts
                $laravel->lock->packages[] = $elem;
                $needComposerInstall = true;
            }
        }


        //add current package to lock
        foreach ($laravel->lock->packages as $key => $package) {
            if ($package->name == $decodedComposerJson->name) {
                unset($laravel->lock->packages[$key]);
            }
        }
        $lockRecord = clone $newRecord->package;
        $lockRecord->type = "library";
        if (isset($decodedComposerJson->autoload)) {
            $lockRecord->autoload = $decodedComposerJson->autoload;
        }
        if (isset($decodedComposerJson->extra)) {
            $lockRecord->extra = $decodedComposerJson->extra;
        }
        $laravel->lock->packages[] = $lockRecord;

        //add updated hash
        $updatedHash = $this->getContentHash(file_get_contents($tmpPath . '/composer.json'));
        $laravel->lock->{"content-hash"} = $updatedHash;
        $laravel->lock->packages = array_values($laravel->lock->packages);

        //save lock
        file_put_contents($tmpPath . '/composer.lock', json_encode($laravel->lock, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT));

        //make composer install if any changes updated

        //download composer
        // check if we have composer phar already
        if (!file_exists($tmpPath . '/composer.phar')) {
            file_put_contents($tmpPath . '/composer.phar', file_get_contents('https://getcomposer.org/composer-stable.phar'));
        }

        //composer install package
        $command = 'export COMPOSER_HOME=' . $tmpPath . '/.composer' . ' && '.config('installer.php', '/usr/bin/php').' ' . $tmpPath . '/composer.phar' . ' install --working-dir=' . $tmpPath . ' --no-scripts --no-plugins';
        $process = Process::fromShellCommandline($command);
        $process->setTimeout(300);
        $process->mustRun();
        $process->wait();
    }

    public static function getContentHash($composerFileContents)
    {
        $content = json_decode($composerFileContents, true);

        $relevantKeys = array(
            'name',
            'version',
            'require',
            'require-dev',
            'conflict',
            'replace',
            'provide',
            'minimum-stability',
            'prefer-stable',
            'repositories',
            'extra',
        );

        $relevantContent = array();

        foreach (array_intersect($relevantKeys, array_keys($content)) as $key) {
            $relevantContent[$key] = $content[$key];
        }
        if (isset($content['config']['platform'])) {
            $relevantContent['config']['platform'] = $content['config']['platform'];
        }

        ksort($relevantContent);

        return md5(json_encode($relevantContent));
    }

    /**
     *
     * @author JJ
     * @since 2021-04-15
     * @return
     */
    public function disasterScript(Request $request)
    {
        updateCenterLoginCheck();

        $scriptPath = realpath(__DIR__ . '/../../../disaster_script.php');
        $disasterScript = file_get_contents($scriptPath);

        $replaces = [
            "PASSWORD" => addslashes(password_hash($request->password, PASSWORD_DEFAULT)),
            "MYSQL_HOST" => config('database.connections.mysql.host'),
            "MYSQL_PORT" => config('database.connections.mysql.port'),
            "MYSQL_DATABASE" => config('database.connections.mysql.database'),
            "MYSQL_LOGIN" => config('database.connections.mysql.username'),
            "MYSQL_PASSWORD" => config('database.connections.mysql.password'),
        ];
        foreach ($replaces as $key => $value) {
            $disasterScript = str_replace('[' . $key . ']', $value, $disasterScript);
        }

        file_put_contents(public_path('/disaster_script.php'), $disasterScript);
    }

    /**
     * uninstall plugin
     * @author JJ
     * @since 2021-04-15
     * @return
     */
    public function uninstall(Request $request, $pluginId)
    {
        updateCenterLoginCheck();

        $plugin = InstalledPackages::find($pluginId);

        if (empty($plugin)) {
            return redirect('/update_center');
        }

        //check permission
        $unwritableFiles = self::checkWritablePermission(base_path('/vendor'));
        if (!empty($unwritableFiles)) {
            echo '<h1>Uninstall aborted. There are permission problems with files:</h1>';
            foreach ($unwritableFiles as $unwritableFile) {
                echo $unwritableFile . "<br>";
            }
            die;
        }

        $tmpPath = storage_path('update_tmp');
        //prepare tmp folder for composer update
        if (!file_exists($tmpPath)) {
            mkdir($tmpPath, 0777, true);
        }

        //download composer
        // check if we have composer phar already
        if (!file_exists($tmpPath . '/composer.phar')) {
            file_put_contents($tmpPath . '/composer.phar', file_get_contents('https://getcomposer.org/composer-stable.phar'));
        }


        //remove package from composer.json
        $laravelComposerJson = json_decode(file_get_contents(base_path('/composer.json')));
        if (isset($laravelComposerJson->require->{$plugin->namespace})) {
            unset($laravelComposerJson->require->{$plugin->namespace});
        }
        $laravelComposerJson->repositories = array_values($laravelComposerJson->repositories);
        file_put_contents(base_path('/composer.json'), json_encode($laravelComposerJson, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT));


        //remove package from composer.lock
        $laravelComposerLock = json_decode(file_get_contents(base_path('/composer.lock')));
        foreach ($laravelComposerLock->packages as $key => $package) {
            if ($package->name == $plugin->namespace) {
                unset($laravelComposerLock->packages[$key]);
            }
        }
        $laravelComposerLock->packages = array_values($laravelComposerLock->packages);
        file_put_contents(base_path('/composer.lock'), json_encode($laravelComposerLock, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT));

        //composer install package to update vendor according to new .lock
        $command = 'export COMPOSER_HOME=' . $tmpPath . '/.composer' . ' && '.config('installer.php', '/usr/bin/php').' ' . $tmpPath . '/composer.phar' . ' install --no-plugins --working-dir=' . base_path();
        dd($command);
        $process = Process::fromShellCommandline($command);
        $process->setTimeout(300);
        $process->mustRun();
        $process->wait();

        $plugin->delete();

        return redirect()->back();
    }

    /**
     * Get dependencies data
     * @author JJ
     * @since 2021-05-14
     * @return array
     */
    public function getDependenciesData($packageData): array
    {
        $tmpPath = storage_path('update_tmp');

        $return = [];
        $changes = [];

        $packageComposerJson = json_decode(base64_decode($packageData->data->composerJson));
        $packageComposerLock = json_decode(base64_decode($packageData->data->composerLock));

        //get laravel composer json && composer.lock
        if (!file_exists($tmpPath . '/composer.json') || !file_exists($tmpPath . '/composer.lock')) {
            echo '<h1>Dependencies installation aborted. Composer files does not exists</h1>';
            die;
        }
        $laravelComposerJson = json_decode(file_get_contents(base_path('/composer.json')));
        $laravelComposerLock = json_decode(file_get_contents(base_path('/composer.lock')));


        $package = new \StdClass();
        $package->json = $packageComposerJson;
        $package->lock = $packageComposerLock;

        $laravel = new \StdClass();
        $laravel->json = $laravelComposerJson;
        $laravel->lock = $laravelComposerLock;

        //scan for changes
        $packageParsedPackages = $this->parsePackages($package->lock->packages);
        $laravelParsedPackages = $this->parsePackages($laravel->lock->packages);

        foreach ($packageParsedPackages as $key => $elem) {
            if (!isset($laravelParsedPackages[$key])) {
                $changes[$key] = $elem;
            }
        }

        return [
            $package,
            $laravel,
            $changes,
        ];
    }


    /**
     * Parse pacages to key => value array
     * @author JJ
     * @since 2021-05-14
     * @param array $packages
     * @return array
     */
    public function parsePackages(array $packages): array
    {
        $packageParsedPackages = [];
        foreach ($packages as $package) {
            $packageParsedPackages[$package->name] = $package;
        }
        return $packageParsedPackages;
    }

    /**
     * Backup composer.json && lock
     * @author JJ
     * @since 2021-05-14
     * @return void
     */
    public function backupComposerJson(): void
    {
        //create main backup folder if not exists
        if (!file_exists(base_path('composer_backups'))) {
            mkdir(base_path('composer_backups'), 0777, true);
        }

        //create folder with datetime
        copy(base_path('composer.json'), base_path('composer_backups/composer.json'));
        copy(base_path('composer.lock'), base_path('composer_backups/composer.lock'));
    }


    public function copyFiles()
    {
        updateCenterLoginCheck();
        $tmpPath = storage_path('update_tmp');
        // dd($tmpPath);
        //rename vendor to vendor old
        if (!file_exists(base_path('composer_backups'))) {
            mkdir(base_path('composer_backups'), 0777, true);
        }

        $this->copyr(base_path('vendor'), base_path('composer_backups/vendor'));
        $this->rrmdir(base_path('vendor'));
        $this->copyr($tmpPath . '/vendor', base_path('vendor'));

        //copy && override also composer files
        $this->backupComposerJson();
        copy($tmpPath . "/composer.json", base_path('composer.json'));
        copy($tmpPath . "/composer.lock", base_path('composer.lock'));

        //cleanup temp folder
        $this->rrmdir($tmpPath . '/vendor');
    }

    /**
     * Force migrations - second step
     * @author JJ
     * @since 2021-04-15
     * @return
     */
    public function runMigrations(int $version_id)
    {
        //check if this migrations has run for the first time
        //it solves the problem with appcore - with middlewares
        $packageData = ApiCall::call('/getPackageData?version=' . $version_id);
        $decodedJsonData = json_decode(base64_decode($packageData->data->composerJson));

        $packageRecord = InstalledPackages::where('namespace', '=', $decodedJsonData->name)
            ->first();
        if (empty($packageRecord)) {
            $packageRecord = new InstalledPackages();
        } else {
            if ($packageRecord->version_id == $packageData->data->releaseData->id) {
                echo 'Post-migration already runned. Skipping...';die;
            }
        }


        $tmpPath = storage_path('update_tmp');

        //check if migration table exists
        if (!Schema::hasTable('migrations')) {
            Artisan::call("migrate:install");
        }

        //force dump autoload on main project
        $command = 'export COMPOSER_HOME=' . $tmpPath . '/.composer' . ' && '.config('installer.php', '/usr/bin/php').' ' . $tmpPath . '/composer.phar' . ' dump-autoload --no-plugins --working-dir=' . base_path();
        $process = Process::fromShellCommandline($command);
        $process->setTimeout(300);
        $process->mustRun();
        $process->wait();

        //publish all data
        Artisan::call("vendor:publish", [
            "--all" => true,
        ]);

        //remove spatie permission migration (always, this tables are generated by appcore)
        $scanned_directory = array_diff(scandir(base_path('database/migrations')), array('..', '.'));
        if (!empty($scanned_directory)) {
            foreach ($scanned_directory as $file) {
                if (stripos($file, "create_permission_tables") !== false) {
                    //make laravel think this migration was already pushed
                    $migrationName = str_replace('.php', '', $file);
                    $check = \DB::table('migrations')
                        ->where('migration', '=', $migrationName)
                        ->first();
                    if (!$check) {
                        \DB::table('migrations')
                            ->insert([
                                'migration' => $migrationName,
                                'batch' => 1
                            ]);
                    }
                }
            }
        }

        //set laravel migrations first
        Artisan::call('migrate', [
            '--force' => true,
            '--no-interaction' => true,
            '--path' => "database/migrations",
        ]);

        //then push rest of migrations
        Artisan::call('migrate', [
            '--force' => true,
            '--no-interaction' => true,
        ]);

        //clear all cache
        Artisan::call('view:clear');
        Artisan::call('config:clear');
        Artisan::call('route:clear');
        Artisan::call('cache:clear');

        //if theres andrew appcore present - call app-update to copy files
        $allCommands = Artisan::all();
        if (isset($allCommands["app-update"])) {
            Artisan::call('app-update');
        }



        $packageRecord->package_id = $packageData->data->packageData->id;
        $packageRecord->namespace = $decodedJsonData->name;
        $packageRecord->name = $packageData->data->packageData->name;
        $packageRecord->version_id = $packageData->data->releaseData->id;
        $packageRecord->version = $packageData->data->releaseData->version;
        $packageRecord->status = "installed";
        $packageRecord->save();
    }

    public function deleteVendorBackup()
    {
        updateCenterLoginCheck();
        $this->rrmdir(base_path("composer_backups"));
    }

    public function copyr($source, $dest)
    {
        //create folder if not exists
        if (!file_exists($dest)) {
            mkdir($dest, 0755, true);
        }

        if (is_dir($source)) {
            $dir_handle = opendir($source);
            while ($file = readdir($dir_handle)) {
                if ($file != "." && $file != "..") {
                    if (is_dir($source . "/" . $file)) {
                        if (!is_dir($dest . "/" . $file)) {
                            mkdir($dest . "/" . $file);
                        }
                        $this->copyr($source . "/" . $file, $dest . "/" . $file);
                    } else {
                        copy($source . "/" . $file, $dest . "/" . $file);
                    }
                }
            }
            closedir($dir_handle);
        } else {
            copy($source, $dest);
        }
    }

    public function rrmdir($dir)
    {
        if (is_dir($dir)) {
            $objects = scandir($dir);
            foreach ($objects as $object) {
                if ($object != "." && $object != "..") {
                    if (is_dir($dir . DIRECTORY_SEPARATOR . $object) && !is_link($dir . "/" . $object)) {
                        $this->rrmdir($dir . DIRECTORY_SEPARATOR . $object);
                    } else {
                        unlink($dir . DIRECTORY_SEPARATOR . $object);
                    }
                }
            }
            rmdir($dir);
        }
    }

    public static function checkWritablePermission($dir)
    {
        $return = [];
        if (is_dir($dir)) {
            $objects = scandir($dir);
            foreach ($objects as $object) {
                if ($object != "." && $object != "..") {
                    if (is_dir($dir . DIRECTORY_SEPARATOR . $object) && !is_link($dir . "/" . $object)) {
                        $return = array_merge(self::checkWritablePermission($dir . DIRECTORY_SEPARATOR . $object), $return);
                    } else {
                        if (!is_writable($dir . DIRECTORY_SEPARATOR . $object)) {
                            $return[] = $dir . DIRECTORY_SEPARATOR . $object;
                        }
                    }
                }
            }

            if (!is_writable($dir)) {
                $return[] = $dir;
            }
        }
        return $return;
    }
}
