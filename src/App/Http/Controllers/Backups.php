<?php
namespace Sailingbyte\Installer\App\Http\Controllers;

use Illuminate\View\View;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Sailingbyte\Installer\Classes\Backuper;
use Symfony\Component\Console\Output\StreamOutput;

class Backups
{
    /**
     * Show updates dashboard
     * @author JJ
     * @since 2021-03-16
     * @return
     */
    public function create()
    {
        updateCenterLoginCheck();
        ini_set('memory_limit', -1);
        ini_set('max_execution_time', 600);

        $stream = fopen("php://output", "w");
        Artisan::call('backup:run --disable-notifications', [], new StreamOutput($stream));
        $data = ob_get_clean();

        $html = view('Installer::parts.create', compact('data'))
            ->render();

        return $html;
    }

    /**
     * Delete backup
     * @author JJ
     * @since 2021-04-15
     * @return
     */
    public function delete(Request $request)
    {
        updateCenterLoginCheck();
        $backupName = base64_decode($request->backup);
        \Storage::delete($backupName);
        return redirect()->back();
    }

}
