<?php
namespace Sailingbyte\Installer\App\Http\Controllers;

use Illuminate\View\View;
use Illuminate\Http\Request;
use Sailingbyte\Installer\Classes\ApiCall;
use Sailingbyte\Installer\App\Models\InstalledPackages;

class Dashboard
{
    /**
     * Show updates dashboard
     * @author JJ
     * @since 2021-03-16
     * @return  
     */
    public function dashboard() : View
    {
        updateCenterLoginCheck();
        //list backups
        $backups = \Storage::disk('local')
            ->files('backup');

        //download plugins data from API
        $pluginsData = ApiCall::call('/getRepositoriesData?dupa=test');
        $pluginsData = $pluginsData->data;
        

        //get currently installed plugins
        $installedPlugins = InstalledPackages::get();

        $availableUpdates = [];
        $pluginsToInstall = [];
        foreach($pluginsData as $elem) {
            $check = $installedPlugins->where('package_id', '=', $elem->id)
                ->first();

            if(empty($check)) {
                //get plugins that you can install
                $pluginsToInstall[] = $elem;
            } else {
                //get available updates
                if($check->version != $elem->version) {
                    $availableUpdates[$elem->namespace] = [
                        "version" => $elem->version,
                        "version_id" => $elem->version_id,
                    ]; 
                }
            }
        }

        

        return view('Installer::dashboard', compact('backups', 'installedPlugins', 'pluginsToInstall', 'availableUpdates','pluginsData'));
    }

    /**
     * Simple Login
     * @author JJ
     * @since 2021-04-19
     * @return  
     */
    public function login() 
    {
        return view('Installer::login');
    }

    /**
     * Login check
     * @author JJ
     * @since 2021-04-19
     * @return  
     */
    public function postLogin(Request $request) 
    {
        $savedKey = config('installer.license_key');
        if(empty($savedKey)) {
            echo 'Insert license into config installer.php first.';
            exit;
        }

        if($request->license_key != $savedKey) {
            return redirect('/update_center/login');
        } else {
            $request->session()->put('update_center_auth', $savedKey);
            return redirect('/update_center');
        }
    }
    
    /**
     * Simple Logout
     * @author JJ
     * @since 2021-04-19
     * @return  
     */
    public function logout(Request $request) 
    {
        $request->session()->put('update_center_auth', null);
        return redirect('/update_center/login');
    }
    
}