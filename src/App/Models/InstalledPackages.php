<?php
namespace Sailingbyte\Installer\App\Models;

use Illuminate\Database\Eloquent\Model;

class InstalledPackages extends Model
{
    protected $table = 'installed_packages';
}
