<?php

session_start();
ini_set('memory_limit', '-1');
ini_set('max_execution_time', '1800');

class Authenticate
{

    //1234
    private static $password = '[PASSWORD]';

    /**
     * Simple auth for disaster script purpose
     * @author JJ
     * @since 2021-04-15
     * @return
     */
    public static function check()
    {
        //check if user click logout
        $logout = $_REQUEST['logout'] ?? null;
        if (!empty($logout)) {
            $_SESSION['disaster_auth'] = null;
            header('Location: /disaster_script.php');
            exit;
        }

        //check if we have password in request
        $passwordInRequest = $_REQUEST['password'] ?? null;
        //if not empty - put into session
        if (!empty($passwordInRequest)) {
            $_SESSION['disaster_auth'] = $passwordInRequest;
        }

        //check if already logged in
        $check = $_SESSION['disaster_auth'] ?? null;
        if (empty($check)) {
            self::showLoginForm();
        }
        if (!password_verify($check, self::$password)) {
            self::showLoginForm();
        }
    }

    /**
     * Show login form
     * @author JJ
     * @since 2021-04-15
     * @return
     */
    public static function showLoginForm()
    {
        echo '
        <form method="post">
            <input type="password" name="password">
            <input type="submit" value="Submit">
        </form>
        ';
        die;
    }

}

class Backups
{

    private static $mysql_host = "[MYSQL_HOST]";

    private static $mysql_port = "[MYSQL_PORT]";

    private static $mysql_database = "[MYSQL_DATABASE]";

    private static $mysql_login = "[MYSQL_LOGIN]";

    private static $mysql_password = "[MYSQL_PASSWORD]";

    private $log = '';

    /**
     * get backups list
     * @author JJ
     * @since 2021-04-15
     * @return
     */
    public static function getBackups()
    {
        if(file_exists('../storage/app/backup')) {
            $backupsFolder = scandir('../storage/app/backup');
        } else {
            $backupsFolder = [];
        }
        return array_values(array_diff($backupsFolder, array('..', '.')));
    }

    /**
     * check if restore has been requested
     * @author JJ
     * @since 2021-04-15
     * @return
     */
    public static function restore()
    {
        $restore = $_REQUEST['restore'] ?? null;
        if (empty($restore)) {
            return;
        }

        //check if file exists
        $restore = base64_decode($restore);
        if (file_exists('../storage/app/backup/' . $restore)) {
            self::unzip($restore, '../storage/app/backup/' . $restore);
            self::restoreFiles($restore, '../storage/app/backup/' . $restore);
            self::restoreDatabase($restore, '../storage/app/backup/' . $restore);
            self::rrmdir('../tmp_restore');

            header('Location: /disaster_script.php?message=' . urlencode('Restored Successful!'));
            exit;
        }

        header('Location: /disaster_script.php?message=' . urlencode('Backup file corrupted or does not exists.'));
        exit;
    }

    /**
     * unzip backup file
     * @author JJ
     * @since 2021-04-15
     * @return
     */
    public static function unzip($restoreFile, $restoreFilePath)
    {
        $zip = new \ZipArchive;
        $res = $zip->open($restoreFilePath);
        if ($res === true) {
            $tmpPath = '../tmp_restore';
            self::rrmdir($tmpPath);
            mkdir($tmpPath, 0777, true);
            // Extract file
            try {
                $zip->extractTo($tmpPath);
                $zip->close();
            } catch (\Exception $e) {
                if (stripos($e->getMessage(), 'Permission denied') !== false) {
                    header('Location: /disaster_script.php?message=' . urlencode('Invalid permissions for Vendor or Storage folder.'));
                    exit;
                } else {
                    return $e;
                }
            }
        }
    }

    /**
     * unzip backup file
     * @author JJ
     * @since 2021-04-15
     * @return
     */
    public static function restoreFiles($restoreFile, $restoreFilePath)
    {
        $projectPath = realpath(__DIR__.'/../');
        if(!$projectPath) {
            header('Location: /disaster_script.php?message=' . urlencode('Problem with files restore - invalid project path.'));
            exit;
        }
        //check if restoration folder exsits
        $restorationFolder = '../tmp_restore'.$projectPath;
        if(!file_exists($restorationFolder)) {
            header('Location: /disaster_script.php?message=' . urlencode('Problem with files restore - invalid restoration path.'));
            exit;
        }

        $checkIfForce = $_REQUEST['force'] ?? null;

        if(empty($checkIfForce)) {
            //check if files are writable
            $permissionFiles = self::checkWritablePermission($projectPath);
            if(!empty($permissionFiles)) {
                $_SESSION['unwritable_files'] = $permissionFiles;
                header('Location: /disaster_script.php?force=true&message=' . urlencode('Problem with files permission - invalid permissions'));
                exit;
            }
        }

        //reset session vars
        $_SESSION['unwritable_files'] = null;
        $_SESSION['skipped_files'] = null;

        // recursive restore files
        set_error_handler("installer_warning_handler", E_WARNING);
        $log = self::copyr($restorationFolder, $projectPath);
        restore_error_handler();
    }

    /**
     * unzip backup file
     * @author JJ
     * @since 2021-04-15
     * @return
     */
    public static function restoreDatabase($restoreFile, $restoreFilePath)
    {
        $mysqli = mysqli_connect(self::$mysql_host, self::$mysql_login, self::$mysql_password, self::$mysql_database, self::$mysql_port);
        $mysqli->query('SET foreign_key_checks = 0');
        if ($result = $mysqli->query("SHOW TABLES")) {
            while ($row = $result->fetch_array(MYSQLI_NUM)) {
                $mysqli->query('DROP TABLE IF EXISTS ' . $row[0]);
            }
        }

        //get dbdump file
        if(file_exists('../tmp_restore/db-dumps/')) {
            $dump = array_values(array_diff(scandir('../tmp_restore/db-dumps/'), array('..', '.')))[0] ?? null;
            if(!empty($dump)) {

                //use custom importer to load large files
                new Import('../tmp_restore/db-dumps/'.$dump, self::$mysql_login, self::$mysql_password, self::$mysql_database, self::$mysql_host);

                if(!empty($mysqlImport->errors)) {
                    header('Location: /disaster_script.php?force=true&message=' . urlencode('Problem with database restore.'));
                    exit;
                }
            }
        }
        $mysqli->query('SET foreign_key_checks = 1');
        $mysqli->close();
    }

    public static function checkWritablePermission($dir)
    {
        $return = [];
        if (is_dir($dir)) {
            $objects = scandir($dir);
            foreach ($objects as $object) {
                if ($object != "." && $object != "..") {
                    if (is_dir($dir . DIRECTORY_SEPARATOR . $object) && !is_link($dir . "/" . $object)) {
                        $return = array_merge(self::checkWritablePermission($dir . DIRECTORY_SEPARATOR . $object), $return);
                    } else {
                        if(!is_writable($dir . DIRECTORY_SEPARATOR . $object)) {
                            $return[] = $dir . DIRECTORY_SEPARATOR . $object;
                        }
                    }
                }
            }

            if(!is_writable($dir)) {
                $return[] = $dir;
            }
        }
        return $return;
    }

    public static function copyr($source, $dest)
    {
        //create folder if not exists
        if (!file_exists($dest)) {
            mkdir($dest, 0755, true);
        }

        if (is_dir($source)) {
            $dir_handle = opendir($source);
            while ($file = readdir($dir_handle)) {
                if ($file != "." && $file != "..") {
                    if (is_dir($source . "/" . $file)) {
                        if (!is_dir($dest . "/" . $file)) {
                            mkdir($dest . "/" . $file);
                        }
                        self::copyr($source . "/" . $file, $dest . "/" . $file);
                    } else {
                        copy($source . "/" . $file, $dest . "/" . $file);
                    }
                }
            }
            closedir($dir_handle);
        } else {
            copy($source, $dest);
        }
    }

    public static function rrmdir($dir)
    {
        if (is_dir($dir)) {
            $objects = scandir($dir);
            foreach ($objects as $object) {
                if ($object != "." && $object != "..") {
                    if (is_dir($dir . DIRECTORY_SEPARATOR . $object) && !is_link($dir . "/" . $object)) {
                        self::rrmdir($dir . DIRECTORY_SEPARATOR . $object);
                    } else {
                        unlink($dir . DIRECTORY_SEPARATOR . $object);
                    }
                }
            }
            rmdir($dir);
        }
    }
}

class Import
{
    private $db;
    private $filename;
    private $username;
    private $password;
    private $database;
    private $host;
    private $port;

    /**
      * instanciate
      * @param $filename string name of the file to import
      * @param $username string database username
      * @param $password string database password
      * @param $database string database name
      * @param $host string address host localhost or ip address
      * @param $port int port for the host, default is 3306
    */
    public function __construct($filename, $username, $password, $database, $host, $port = 3306)
    {
        //set the varibles to properties
        $this->filename = $filename;
        $this->username = $username;
        $this->password = $password;
        $this->database = $database;
        $this->host = $host;
        $this->port = $port;

        //connect to the datase
        $this->connect();

        //open file and import the sql
        $this->openfile();
    }

    /**
     * Connect to the database
    */
    protected function connect()
    {
        $this->db = $this->createconnection();
        if ($this->db->connect_errno) {
            throw new Exception("Failed to connect to MySQL: " . $this->db->connect_error);
        }
    }

    /**
     * run queries
     * @param string $query the query to perform
    */
    protected function query($query)
    {
        if (!$this->db->query($query)) {
            throw new Exception("Error with query: ".$this->db->error."\n");
        }
    }

    /**
     * Open $filename, loop through and import the commands
    */
    protected function openfile()
    {
        try {
            //if file cannot be found throw errror
            if (!file_exists($this->filename)) {
                throw new Exception("Error: File not found.\n");
            }

            // Read in entire file
            $fp = fopen($this->filename, 'r');

            // Temporary variable, used to store current query
            $templine = '';

            // Loop through each line
            while (($line = fgets($fp)) !== false) {
                // Skip it if it's a comment
                if (substr($line, 0, 2) == '--' || $line == '') {
                    continue;
                }

                // Add this line to the current segment
                $templine .= $line;

                // If it has a semicolon at the end, it's the end of the query
                if (substr(trim($line), -1, 1) == ';') {
                    $this->query($templine);

                    // Reset temp variable to empty
                    $templine = '';
                }
            }

            //close the file
            fclose($fp);
        } catch (Exception $e) {
            echo "Error importing: ".$e->getMessage()."\n";
        } finally {
            $this->db->close();
        }
    }

    /**
     * @codeCoverageIgnore
     */
    protected function createconnection()
    {
        return new mysqli($this->host, $this->username, $this->password, $this->database, $this->port);
    }
}

function installer_warning_handler($errno, $errstr) {
    if(empty($_SESSION['skipped_files'])) {
        $_SESSION['skipped_files'] = [];
    }
    $_SESSION['skipped_files'][] = $errstr;
}

Authenticate::check();
$backups = Backups::getBackups();
Backups::restore();
?>

<a href="/disaster_script.php?logout=true">Logout</a>

<?php
    $message = $_REQUEST['message'] ?? null;
    if (!empty($message)) {
        echo "<h3>" . urldecode($message) . "</h3>";
    }
?>

<?php
    $unwritableFiles = $_SESSION['unwritable_files'] ?? null;
    if (!empty($unwritableFiles)) :
?>
    <br>Unwritable files: <br>
<?php
    foreach($unwritableFiles as $file) {
        echo $file."<br>";
    }
?>
    <br>
<?php
    endif;
?>

<?php
    $skippedFiles = $_SESSION['skipped_files'] ?? null;
    if (!empty($skippedFiles)) :
?>
    <br>Last restoration skipped files: <br>
<?php
    foreach($skippedFiles as $file) {
        echo $file."<br>";
    }
?>
    <br>
<?php
    endif;
?>

<table>
    <thead>
        <tr>
            <td>Backup</td>
            <td>Action</td>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($backups as $backup): ?>
        <tr>
            <td><?php echo $backup ?></td>
            <td>
                <?php
                    if (empty($unwritableFiles)) :
                ?>
                    <a href="/disaster_script.php?restore=<?php echo base64_encode($backup) ?>">Restore</a>
                <?php
                    else :
                ?>
                    <a href="/disaster_script.php?force=true&restore=<?php echo base64_encode($backup) ?>">Force Restore and skip listed files</a>
                <?php
                    endif;
                ?>
            </td>
        </tr>
        <?php endforeach;?>
    </tbody>
</table>


